// Two spring-damper-mass systems connected in series
//
// Author:  Anita Sant'Anna, 2012
//
// Run this example, view Plot, and then 3D

// This example makes use of the spring-damper-mass system
// created in ~\examples\2_Modeling_Physical_Systems
//
// This example creates two spring-damper-mass systems. The first one is 
// connected to a fixed point at one end, and to the other system
// at the other end. 

// Each spring-damper-mass system is modeled by the system of ODEs: 
//
//    Fs  = k*(x0-x) - c*x';
//    x'' = (Fs - F)/m;
//      where:
//      Fs: force exerted on mass by spring and damper
//      F: external force applied to the mass (compressing the spring)
//      k: spring constant
//      x0: initial position of the mass
//      x: position of the mass
//      c: damper coefficient
//      m: mass


class SpringDamper(c,k,m,x0)
  private x:=x0; x':=0; x'':=0; F:=0; Fs:=0; 
    _3D:=[["Box",[x0,0,0],[0.1*m,0.1*m,0.1*m],[0,0,1],[0,0,0]],
         ["Cylinder",[x0-0.5,0,0],[0.01,1],[0,1,0],[0,0,3.1416/2]]];
  end
  Fs= k*(x0-x) - c*x';
  x''=(Fs - F)/m;
   _3D=[["Box",[x,0,0],[0.1*m,0.1*m,0.1*m],[0,0,1],[0,0,0]],
         ["Cylinder",[0.5+(x-x0)/2-(1-x0),0,0],[0.01,-1-(x-x0)],[0,1,0],[0,0,3.1416/2]]];
end

class Main(simulator)
  private 
    mode:="push"; t:=0; t':=1; x01:=0; x02:=1;    
    // create two systems with different masses
    // note that the initial position of system2 is 1 unit apart from that of system1
    system1 := create SpringDamper(1,5,3,0);  
    system2 := create SpringDamper(1,5,1,1);  
    _3D:=["Cylinder",[-1,0,0],[0.01,1],[1,0,0],[3.1416/2,0,0]];     
  end

  // connecting the two systems in parallel
  // position of system2 depends on the position of system1 as described below
  system2.x0=x02+system1.x-x01;
  // the force exerted by the spring of system2 is an external force for system1
  system1.F=system2.Fs;
  t'=1;
  _3D=["Cylinder",[x01-1,0,0],[0.01,1],[1,0,0],[3.1416/2,0,0]]; 

  // a force F=2 is applied to system2 for one second
  switch mode
    case "push"
    system2.F=2;   
    if t>=1
      mode:="free_motion";
    end

    case "free_motion"
    system2.F=0;
  end
end

